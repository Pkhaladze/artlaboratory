<?php 
return [
    'material' => [
        'gold'   => 'ოქრო',
        'silver' => 'ვერცხლი',
        'enamel' => 'მინანქარი'
    ],
    'author' => [
        'Tamuna Tsnoreli'  => 'თამუნა წნორელი',
        'Natia Nebieridze' => 'ნათია ნებიერიძე',
    ],
    'category' => [
        'earring'   => 'საყურე',
        'necklace'  => 'ყელსაბამი',
        'brooches'  => 'გულსაკიდი',
        'ring'      => 'ბეჭედი',
        'bracelet'  => 'სამაჯური',
        'exclusive' => 'ექსკლუზივი',
        'gold'   => 'ოქრო',
        'silver' => 'ვერცხლი',
    ]

]

 ?>