@php
    use App\Models\Item;
    $newest_items = Item::where('status', 'active')->orderBy('id', 'desc')->take(3)->get();
@endphp
<div class="cell-md-4 offset-top-45 offset-md-top-0">
    <h3 class="text-center text-sm-left">ახალი პროდუქტები</h3>
    <hr class="divider divider-sm-left divider-base divider-bold">
    @foreach ($newest_items as $item)
        <div class="range offset-top-20">
            <div class="cell-md-12 cell-sm-4">
                <div class="unit unit-horizontal unit-spacing-21">
                    <div class="unit-left">
                        <a href="{{ url('products/show/' . $item->id . '/item') }}">
                            <img alt="" src="{{ url('item/img/' . $item->id . '/' . $item->photos->first()->fileName) }}" width="100" height="100">
                        </a>
                    </div>
                    <div class="unit-body">
                        <div class="p">
                            <a href="{{url('products/show/' . $item->id . '/item')}}">{{$item->category }}</a>
                        </div>
                        <div class="big offset-top-4">
                            <a href="{{url('products/show/' . $item->id . '/item')}}" class="text-base">{{$item->name}}</a>
                        </div>
                        <div class="offset-top-4">
                            <div class="product-price text-bold">
                                @if ($item->sale)
                                    {{$item->sale}}<i class="lari lari-normal"></i>
                                    <span class="font-default text-light text-muted text-strike small">{{$item->cost}}</span>
                                @else 
                                    {{$item->cost}}<i class="lari lari-normal"></i>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach             
</div>