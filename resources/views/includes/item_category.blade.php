@php  $items->where('status', 'active');  @endphp

<div class="shell section-bottom-60 offset-top-20">
  <div class="range">
    <div class="cell-md-3 cell-sm-4 cell-xs-6">
        <a href="{{ url('products/earring')}}" class="thumbnail-variant-1 reveal-block">
            <img alt="" src="{{ url('images/category/1/products-01.jpg') }}" width="270" height="270" class="img-responsive">
            <div class="caption">
                <h5 class="caption-title">საყურეები</h5>
                <p class="caption-descr">{{$items->where('category', 'earring')->count()}} პროდუქტი</p>
            </div>
        </a>
    </div>
    <div class="offset-top-30 offset-xs-top-0 cell-md-3 cell-sm-4 cell-xs-6">
        <a href="{{ url('products/necklace') }}" class="thumbnail-variant-1 reveal-block">
        <img alt="" src="{{ url('images/category/2/products-02.jpg') }}" width="270" height="270" class="img-responsive">
        <div class="caption">
          <h5 class="caption-title">ყელსაბამები</h5>
          <p class="caption-descr">{{$items->where('category', 'necklace')->count()}}
        </div>
      </a>
    </div>
      <div class="offset-top-30 offset-sm-top-0 cell-md-3 cell-sm-4 cell-xs-6">
        <a href="{{ url('products/brooches') }}" class="thumbnail-variant-1 reveal-block">
          <img alt="" src="{{ url('images/category/3/products-03.jpg') }}" width="270" height="270" class="img-responsive">
          <div class="caption">
            <h5 class="caption-title">გულსაკიდები</h5>
            <p class="caption-descr">{{$items->where('category', 'brooches')->count()}}
          </div>
        </a>
      </div>
      <div class="offset-top-30 offset-md-top-0 cell-md-3 cell-sm-4 cell-xs-6">
        <a href="{{ url('products/ring') }}" class="thumbnail-variant-1 reveal-block"><img alt="" src="{{ url('images/category/4/products-04.jpg') }}" width="270" height="270" class="img-responsive">
          <div class="caption">
            <h5 class="caption-title">ბეჭდები</h5>
            <p class="caption-descr">{{$items->where('category', 'ring')->count()}}
          </div>
          </a>
      </div>
      <div class="offset-top-30 cell-md-3 cell-sm-4 cell-xs-6">
        <a href="{{ url('products/bracelet') }}" class="thumbnail-variant-1 reveal-block"><img alt="" src="{{ url('images/category/5/products-05.jpg') }}" width="270" height="270" class="img-responsive">
          <div class="caption">
            <h5 class="caption-title">სამაჯურები</h5>
            <p class="caption-descr">{{$items->where('category', 'bracelet')->count()}}
          </div>
        </a>
      </div>
      <div class="offset-top-30 cell-md-3 cell-sm-4 cell-xs-6">
        <a href="{{ url('products/exclusive') }}" class="thumbnail-variant-1 reveal-block"><img alt="" src="{{ url('images/category/6/products-06.jpg') }}" width="270" height="270" class="img-responsive">
          <div class="caption">
            <h5 class="caption-title">ექსკლუზივი</h5>
            <p class="caption-descr">{{$items->where('category', 'exclusive')->count()}}
          </div>
        </a>
      </div>
      <div class="offset-top-30 cell-md-3 cell-sm-4 cell-xs-6">
        <a href="{{ url('products/catalog/silver') }}" class="thumbnail-variant-1 reveal-block">
          <img alt="" src="{{ url('images/category/7/products-07.jpg') }}" width="270" height="270" class="img-responsive">
          <div class="caption">
            <h5 class="caption-title">ვერცხლის ნაკეთობა</h5>
            <p class="caption-descr">{{$items->where('material', 'silver')->count()}}
          </div>
        </a>
      </div>
      <div class="offset-top-30 cell-md-3 cell-sm-4 cell-xs-6">
        <a href="{{ url('products/catalog/gold') }}" class="thumbnail-variant-1 reveal-block">
          <img alt="" src="{{ url('images/category/8/products-08.jpg') }}" width="270" height="270" class="img-responsive">
          <div class="caption">
            <h5 class="caption-title">ოქროს ნაკეთობა</h5>
            <p class="caption-descr">{{$items->where('material', 'gold')->count()}}
          </div>
        </a>
      </div>
    </div>
  </div>