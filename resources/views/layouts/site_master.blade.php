<html lang="en" class="wide wow-animation desktop landscape rd-navbar-fixed-linked">
<head>
    <title>Artlaboratory</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic%7CRoboto:400,300,100,700,300italic,400italic,700italic%7CMontserrat:400,700">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-larisome.min.css') }}">
    <link rel="stylesheet" href="{{ url('fonts/bpg_nino/bpg_nino.css') }}">
    <link rel="stylesheet" href="{{ url('css/global.css') }}">
    </head>
<body>
    <div class="page text-center">
        <header class="page-header">
            <div class="rd-navbar-wrap" style="height: 130px;">
                <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fullwidth" data-lg-layout="rd-navbar-static" class="rd-navbar rd-navbar-static" data-stick-up-offset="50" data-md-layout="rd-navbar-fullwidth">
                    <div class="rd-navbar-toppanel">
                        <div class="rd-navbar-toppanel-inner">
                            <div class="rd-navbar-toppanel-submenu"></div>
                            <div class="rd-navbar-toppanel-wrapper">
                                <div class="rd-navbar-contact-info">
                                    <p class="text-black text-regular">
                                        Call us: 
                                        <a class="text-black" href="callto:#">+995 (595) 88 57 48 / </a>
                                        <span class="text-black text-light text-italic">7 Days a week from 9:00 am to 7:00 pm</span>
                                    </p>
                                </div>
                                <div class="rd-navbar-toppanel-search">
                                    <div class="rd-navbar-search-wrap"></div>
                                </div>
                                <div class="rd-navbar-currency"></div>
                            </div>
                        </div>
                    </div>
                    <div class="rd-navbar-inner">
                        <div class="rd-navbar-panel">
                            <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle">
                                <span></span>
                            </button>
                            <div class="rd-navbar-brand">
                                <a href="./" class="brand-name">
                                    <img alt="" src="{{ url('images/logo.png') }}">
                                </a>
                            </div>
                            <div class="rd-navbar-elements-wrap text-right">
                                <ul class="rd-navbar-socials elements-group-18 reveal-inline-block text-middle">
                                    <li>
                                        <a href="https://www.facebook.com/ArTLabEnamel" class="text-gray icon icon-xs fa-facebook" target="_blank"></a>
                                    </li>
                                    <li><a href="#" class="text-gray icon icon-xs fa-google-plus"></a></li>
                                </ul>
                        </div>
                        <div class="rd-navbar-nav-wrap">
                            <ul class="rd-navbar-nav bpg_nino_mtavruli">
                                <li class="{{ isset($active_link) && $active_link == 'home' ? 'active' : '' }}"><a class="main_menu" href="{{ url('./') }}">მთავარი</a></li>
                                <li class="{{ isset($active_link) && $active_link == 'products' ? 'active' : '' }} rd-navbar--has-dropdown rd-navbar-submenu">
                                    <a href="{{ url('/products') }}">პროდუქტი</a>
                                    <ul class="rd-navbar-dropdown">
                                        <li class="rd-navbar--has-dropdown rd-navbar-submenu">
                                            <a href="{{url('products/catalog/gold')}}">ოქრო</a>
                                            <ul class="rd-navbar-dropdown">
                                                <li><a href="{{url('products/gold/earring')}}">საყურეები</a></li>
                                                <li><a href="{{url('products/gold/necklace')}}">ყელსაბამები</a></li>
                                                <li><a href="{{url('products/gold/brooches')}}">გულსაკიდები</a></li>
                                                <li><a href="{{url('products/gold/ring')}}">ბეჭდები</a></li>
                                                <li><a href="{{url('products/gold/bracelet')}}">სამაჯური</a></li>
                                                <li><a href="{{url('products/gold/exclusive')}}">ექსკლუზივი</a></li>     
                                            </ul>
                                        </li>
                                        <li class="rd-navbar--has-dropdown rd-navbar-submenu">
                                            <a href="{{url('products/catalog/silver')}}" >ვერცხლი</a>
                                            <ul class="rd-navbar-dropdown">
                                                <li><a href="{{url('products/silver/earring')}}">საყურეები</a></li>
                                                <li><a href="{{url('products/silver/necklace')}}">ყელსაბამები</a></li>
                                                <li><a href="{{url('products/silver/brooches')}}">გულსაკიდები</a></li>
                                                <li><a href="{{url('products/silver/ring')}}">ბეჭდები</a></li>
                                                <li><a href="{{url('products/silver/bracelet')}}">სამაჯური</a></li>
                                                <li><a href="{{url('products/silver/exclusive')}}">ექსკლუზივი</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="{{ isset($active_link) && $active_link == 'gallery' ? 'active' : '' }}"><a href="{{ url('/gallery') }}">გალერეა</a></li>
                                <li class="{{ isset($active_link) && $active_link == 'about' ? 'active' : '' }}"><a href="{{ url('about') }}">ჩვენს შესახებ</a></li>
                                <li class="{{ isset($active_link) && $active_link == 'contact' ? 'active' : '' }}"><a href="{{ url('contacts') }}">კონტაქტი</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </header>


        @yield('content')

        <footer class="page-footer section-60">
            <div class="shell">
                <a href="./" class="brand">
                    <img alt="" src="{{ url('images/logo.png')}}" width="163" height="41" class="reveal-inline-block img-responsive">
                </a>
                <p >Our products are a combination of classic and modern style; we can satisfy any demands of customers, that is why we have so many fans. You will 
                    <br class="veil reveal-lg-block"> always be popular with our jewelry; the magical shine of our products will bring you the best of luck!
                </p>
                <ul class="elements-group-20 offset-top-20">
                    <li><a href="#" class="icon icon-xs text-base fa-facebook"></a></li>
                    <li><a href="#" class="icon icon-xs text-base fa-twitter"></a></li>
                    <li><a href="#" class="icon icon-xs text-base fa-google-plus"></a></li>
                    <li><a href="#" class="icon icon-xs text-base fa-linkedin"></a></li>
                    <li><a href="#" class="icon icon-xs text-base fa-pinterest"></a></li>
                </ul>
                <p class="offset-top-20 text-muted"><span class="text-bold">Turquoise</span> 2017 | <a href="privacy.html">Privacy Policy</a></p>
            </div>
        </footer>
    </div>

    <script src="{{ url('js/core.min.js') }}"></script>
    <script src="{{ url('js/script.js') }}"></script>
    <!-- begin olark code-->
    <script data-cfasync="false" type="text/javascript">
      window.olark || (function (c) {
        var f = window, d = document, l = f.location.protocol == "https:" ? "https:" : "http:", z = c.name, r = "load";
        var nt = function () {
            f[z] = function () {
                (a.s = a.s || []).push(arguments)
            };
            var a = f[z]._ = {}, q = c.methods.length;
            while (q--) {
                (function (n) {
                    f[z][n] = function () {
                        f[z]("call", n, arguments)
                    }
                })(c.methods[q])
            }
            a.l = c.loader;
            a.i = nt;
            a.p = {0: +new Date};
            a.P = function (u) {
                a.p[u] = new Date - a.p[0]
            };
            function s() {
                a.P(r);
                f[z](r)
            }
      
            f.addEventListener ? f.addEventListener(r, s, false) : f.attachEvent("on" + r, s);
            var ld = function () {
                function p(hd) {
                    hd = "head";
                    return ["<", hd, "></", hd, "><", i, ' onl' + 'oad="var d=', g, ";d.getElementsByTagName('head')[0].", j, "(d.", h, "('script')).", k, "='", l, "//", a.l, "'", '"', "></", i, ">"].join("")
                }
      
                var i = "body", m = d[i];
                if (!m) {
                    return setTimeout(ld, 100)
                }
                a.P(1);
                var j = "appendChild", h = "createElement", k = "src", n = d[h]("div"), v = n[j](d[h](z)), b = d[h]("iframe"), g = "document", e = "domain", o;
                n.style.display = "none";
                m.insertBefore(n, m.firstChild).id = z;
                b.frameBorder = "0";
                b.id = z + "-loader";
                if (/MSIE[ ]+6/.test(navigator.userAgent)) {
                    b.src = "javascript:false"
                }
                b.allowTransparency = "true";
                v[j](b);
                try {
                    b.contentWindow[g].open()
                } catch (w) {
                    c[e] = d[e];
                    o = "javascript:var d=" + g + ".open();d.domain='" + d.domain + "';";
                    b[k] = o + "void(0);"
                }
                try {
                    var t = b.contentWindow[g];
                    t.write(p());
                    t.close()
                } catch (x) {
                    b[k] = o + 'd.write("' + p().replace(/"/g, String.fromCharCode(92) + '"') + '");d.close();'
                }
                a.P(2)
            };
            ld()
        };
        nt()
      })({
        loader: "static.olark.com/jsclient/loader0.js",
        name: "olark",
        methods: ["configure", "extend", "declare", "identify"]
      });
    </script>
</body>
</html>