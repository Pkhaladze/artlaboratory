<?php use App\Models\Item;?>
@extends('layouts.admin_master')
@section('content')
<div class="container">
   @if (session()->has('success'))
        <div class="col-md-offset-2 col-md-8" >
              <div class="alert alert-success">
                <strong>{{ session('success') }}</strong>
              </div>
        </div>
    @endif
    @if (isset($errors) && count($errors) > 0)
        <ul class="list-group">
            @foreach ($errors->all() as $error)
                <li class="list-group-item list-group-item-danger">
                    <strong>{{ $error }}</strong>
                </li>
            @endforeach
        </ul>
    @endif
    @php
        if (isset($item)) {
            $edit = $item;
            $edit->link = $item->id;
        }
        else { 
            $edit = new item;
            $edit->link = "";
        }
    @endphp
    <form method="POST" enctype="multipart/form-data" action="{{url('admin/items/' . $edit->link)}}">
        {{ csrf_field() }}
        <div class="form-horizontal col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <fieldset>
                <legend>პროდუქტის დამატება</legend>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label class="col-md-4 control-label">სახელი</label>  
                    <div class="col-md-6">
                        <input  name="name" value="{{$edit->name}}" type="text" placeholder="name" class="form-control input-md">  
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label class="col-md-4 control-label">წონა</label>  
                    <div class="col-md-6">
                        <input  name="weight" value="{{$edit->weight}}" type="text" placeholder="weight" class="form-control input-md">  
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label class="col-md-4 control-label">სიგრძე</label>  
                    <div class="col-md-6">
                        <input  name="length" value="{{$edit->length}}" type="text" placeholder="length" class="form-control input-md">  
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label class="col-md-4 control-label">სიგანე</label>  
                    <div class="col-md-6">
                        <input  name="width" value="{{$edit->width}}" type="text" placeholder="width" class="form-control input-md">  
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label class="col-md-4 control-label">ფასი</label>  
                    <div class="col-md-6">
                        <input  name="cost" value="{{$edit->cost}}" type="text" placeholder="Cost" class="form-control input-md">  
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label class="col-md-4 control-label">ფასდაკლება</label>  
                    <div class="col-md-6">
                        <input  name="sale" value="{{$edit->sale}}" type="text" placeholder="Sale" class="form-control input-md">  
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label class="col-md-4 control-label">ლითონი</label>  
                    <div class="col-md-6">
                       <select class="form-control" id="materials_select" name="material">
                         <option></option>
                         <option {{$edit->material == 'gold' ? "selected='selected'":"" }} value="gold">ოქრო</option>
                         @if (isset($item))
                            <option {{$edit->material == 'silver' ? "selected='selected'":"" }} value="silver">ვერცხლი</option>
                         @else
                            <option selected='selected' value="silver">ვერცხლი</option>
                         @endif
                         {{-- <option {{$edit->material == 'enamel' ? "selected='selected'":"" }} value="enamel">მინანქარი</option> --}}
                       </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label class="col-md-4 control-label">ავტორი</label>  
                    <div class="col-md-6">
                       <select class="form-control" id="authors_select" name="author">
                         <option></option>
                         @if (isset($item))
                            <option {{$edit->author == 'Tamuna Tsnoreli' ? "selected='selected'":"" }} value="Tamuna Tsnoreli">თამუნა წნორელი</option>
                         @else
                            <option selected='selected' value="Tamuna Tsnoreli">თამუნა წნორელი</option>
                         @endif{{-- 
                         <option {{$edit->author == 'Natia Nebieridze' ? "selected='selected'":"" }} value="Natia Nebieridze">ნათია ნებიერიძე</option> --}}
                       </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label class="col-md-4 control-label">მაღაზია</label>  
                    <div class="col-md-6">
                       <select class="form-control" id="stores_select" name="store_id">
                            <option></option>
                                @if (isset($item))
                                    @foreach ($stores as $store)
                                        <option {{$edit->store_id == $store->id ? "selected='selected'":"" }} value="{{$store->id}}">{{$store->name}} | {{$store->location}}</option>
                                    @endforeach
                                @else
                                    @foreach ($stores as $store)
                                        <option {{$store->id == 1 ? "selected='selected'":"" }} value="{{$store->id}}">{{$store->name}} | {{$store->location}}</option>
                                    @endforeach
                                @endif
                       </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label class="col-md-4 control-label">კატეგორია</label>  
                    <div class="col-md-6">
                        <select class="form-control" id="categories_select" name="category">
                            <option></option>
                            <option {{$edit->category == 'earring' ? "selected='selected'":"" }} value="earring">საყურე</option>
                            <option {{$edit->category == 'necklace' ? "selected='selected'":"" }} value="necklace">ყელსაბამი</option>
                            <option {{$edit->category == 'brooches' ? "selected='selected'":"" }} value="brooches">გულსაბნევი</option>
                            <option {{$edit->category == 'ring' ? "selected='selected'":"" }} value="ring">ბეჭედი</option>
                            <option {{$edit->category == 'bracelet' ? "selected='selected'":"" }} value="bracelet">სამაჯური</option>
                            <option {{$edit->category == 'exclusive' ? "selected='selected'":"" }} value="exclusive">ექსკლუზივი</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label class="col-md-4 control-label">სტატუსი</label>  
                    <div class="col-md-6">
                        <select class="form-control" id="status_select" name="status">
                        <option></option>
                        @if (isset($item))
                            <option {{$edit->status == 'active' ? "selected='selected'":"" }} value="active">აქტიური</option>
                         @else
                            <option selected='selected' value="active">აქტიური</option>
                         @endif
                            <option {{$edit->status == 'passive' ? "selected='selected'":"" }} value="passive">პასიური</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="col-md-2 control-label">აღწერილობა</label>  
                    <div class="col-md-9">
                        <textarea name="description" value="" type="text" placeholder="description" class="form-control" rows="3">{{$edit->description}}
                        </textarea>
                    </div>
                </div>
                <!-- file upload -->
                @if (!isset($item))
                    @for ($i = 1; $i <= 5; $i++)
                        <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-5">
                            <label class="col-xs-12 col-sm-6 col-md-6 col-lg-4">Photo-{{$i}}</label>
                            <input class="col-xs-12 col-sm-6 col-md-6 col-lg-8" type="file" name="photo-{{$i}}">
                        </div>
                    @endfor
                @endif
                <!-- Button -->
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="col-sm-8 col-md-8 col-lg-8 control-label" for="add"></label>
                    <div class="col-md-4">
                        @if ($edit->link == '')
                            <button id="add" name="add" class="btn btn-primary">დამატება</button>
                        @else 
                            <button id="add" name="add" class="btn btn-primary">განახლება</button>
                            <input type="hidden" value="put" name="_method">
                        @endif
                    </div>
                </div>
            </fieldset>
        </div>
    </form>
</div>
@endsection