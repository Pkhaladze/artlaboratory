@extends('layouts.admin_master')
@section('content')
<div class="container">
    <div  class="table-responsive">
        <a class="btn btn-primary pull-right" href="{{url('admin/items/create')}}" role="button">დამატება</a>
        <table class="table table-striped table-hover"> 
            <thead> 
                <tr> 
                    <th>#</th>
                    <th>სახელი</th> 
                    <th>წონა</th> 
                    <th>სიგრძე</th>
                    <th>სიგანე</th>
                    <th>ფასი</th>
                    <th>ფასდაკლება</th>
                    <th>ლითონი</th>
                    <th>ავტორი</th>
                    <th>მაღაზია</th>
                    <th>კატეგორია</th>
                    <th>აღწერა</th>
                    <th>status</th>
                    <th colspan="2">Actions</th> 
                </tr>
            </thead>
            <tbody>
                @php   $i = 0;   @endphp
                @foreach($items as $item)
                    @php  $i++;   @endphp 
                    <tr> 
                    {{-- {{ dd('admin.name', 'Home') }} --}}
                        <td scope="row">{{$i}}</td>  
                        <td>{{$item->name}}</td>
                        <td>{{$item->weight}}</td>
                        <td>{{$item->length}}</td>
                        <td>{{$item->width}}</td>
                        <td>{{$item->cost}}</td>
                        <td>{{$item->sale}}</td>
                        <td>{{ config('admin_tr.material.' . $item->material) }}</td>
                        <td>{{ config('admin_tr.author.' . $item->author) }}</td>
                        <td>{{$item->store->name}} | {{$item->store->location}}</td>
                        <td>{{ config('admin_tr.category.' . $item->category) }}</td>
                        <td>{{$item->description}}</td>
                        <td>
                            <div class="label label-{{$item->status == 'active' ? 'success' : 'danger'}}">
                                {{$item->status}}
                            </div>
                        </td>



                        <td>
                            <a href="{{url('admin/items/'.$item->id.'/edit')}}">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        </td>
                        <td>
                            <form method="POST" action="{{url('admin/items/'.$item->id)}}">
                                <button type="submit" style="background: transparent; border: 0;" id="singlebutton" name="singlebutton" ><i class="glyphicon glyphicon-remove-circle"></i></button>
                                <input type="hidden" value="delete" name="_method">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>
                        </td> 
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
