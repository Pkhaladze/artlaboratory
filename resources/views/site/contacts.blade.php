@extends('layouts.site_master')
@section('content')

<main class="page-content">
    <div class="shell">
        <div>
            <ol class="breadcrumb">
                <li><a href="./" class="icon icon-sm fa-home text-primary"></a></li>
                <li class="active">კონტაქტი</li>
            </ol>
        </div>
    </div>
    <div class="shell section-bottom-60">
        <div class="range">
            <div class="cell-md-8 text-xs-left">
                <h4>საკონტაქტო ინფორმაცია</h4>
                <p>We are always ready to help you. There are many ways to contact us. You may drop us a line, give us a call or send an email, choose what suits you most.</p>
                <dl class="list-terms">
                    <dt>მისამართი</dt>
                    <dd>The Company Name Inc. 9870 St Vincent Place, Glasgow, DC 45 Fr 45.</dd>
                </dl>
                <dl class="list-terms">
                    <dt>ტელეფონი</dt>
                    <dd><a href="tel:+995" class="text-base">+995 </a></dd>
                </dl>
                <dl class="list-terms">
                    <dt>E-mail</dt>
                    <dd><a href="mailto:ttsnoreli@gmail.com">ttsnoreli@gmail.com</a></dd>
                </dl>
                <hr class="divider divider-offset-lg divider-gray">
                @if (session()->has('success'))
                    <ul class="list-group">
                          <li class="list-group-item list-group-item-success">
                            <strong>{{ session('success') }}</strong>
                          </li>
                    </ul>
                @endif
                @if (isset($errors) && count($errors) > 0)
                    <ul class="list-group">
                        @foreach ($errors->all() as $error)
                            <li class="list-group-item list-group-item-danger">
                                <strong>{{ $error }}</strong>
                            </li>
                        @endforeach
                    </ul>
                @endif
                <h4>Contact form</h4>
 
                <form data-result-class="rd-mailform-validate" data-form-type="contact" method="post" action="{{url('/message')}}" class="offset-top-20 rd-mailform">
                    {{ csrf_field() }}
                    <label for="name" class="text-italic">სახელი, გვარი:<span class="text-primary">*</span></label>
                    <div class="mfInput">
                        <input id="name" type="text" name="name" data-constraints="" placeholder="სახელი, გვარი">
                        <div class=""></div>
                    </div>
                    <label for="email" class="text-italic">E-mail:<span class="text-primary">*</span></label>
                    <div class="mfInput">
                        <input id="email" type="text" name="email" data-constraints="" placeholder="ელ. ფოსტა">
                        <div class=""></div>
                    </div>
                    <label for="message" class="text-italic">კითხვა:<span class="text-primary">*</span></label>
                    <div class="mfInput">
                        <textarea id="message" name="question" data-constraints=""></textarea>
                        <div class=""></div>
                    </div>
                    <button class="btn btn-primary bpg_nino_mtavruli">გაგზავნა</button>
                </form>
                <div class="rd-mailform-validate"></div>
            </div>
            <div class="cell-md-3 cell-md-preffix-1 sidebar sidebar-right text-md-left">
                <div class="range">
                    <div class="cell-md-12 cell-sm-6 offset-top-0 offset-sm-top-45 offset-md-top-0">
                        <h4>კატეგორიები</h4>
                        <ul class="offset-top-20 list-dividers">
                            <li><a href="{{ url('products/earring')}}">საყურეები</a></li>
                            <li><a href="{{ url('products/necklace')}}">ყელსაბამები</a></li>
                            <li><a href="{{ url('products/brooches')}}">გულსაკიდები</a></li>
                            <li><a href="{{ url('products/ring')}}">ბეჭდები</a></li>
                            <li><a href="{{ url('products/bracelet')}}">სამაჯური</a></li>
                            <li><a href="{{ url('products/exclusive')}}">ექსკლუზივი</a></li>     
                        </ul>
                        <hr class="divider divider-offset-lg divider-gray veil-sm reveal-md-block">
                    </div>
                    <div class="cell-md-12 offset-top-0 offset-sm-top-45 text-left offset-md-top-0">
                        <h4 class="text-center text-md-left">Recent Reviews</h4>
                        <div class="range offset-top-20">
                            @foreach ($recent_show as $item)
                                <div class="offset-top-20 unit unit-horizontal unit-spacing-21">
                                  <div class="unit-left">
                                    <a href="{{ url('products/show/' . $item->id . '/item') }}"><img alt="" src="{{ url('item/img/' . $item->id . '/' . $item->photos->first()->fileName) }}" width="100" height="100"></a>
                                  </div>
                                  <div class="unit-body">
                                    <div class="p">
                                      <a href="{{url('products/' . $item->category)}}">{{$item->category}}</a>
                                    </div>
                                    <div class="big offset-top-4">
                                      <a href="{{url('products/show/' . $item->id . '/item')}}" class="text-base">{{$item->name}}</a></div>
                                    <div class="offset-top-4">

                                      @if ($item->sale)
                                          {{$item->sale}}<i class="lari lari-normal"></i>
                                          <span class="font-default text-light text-muted text-strike small">{{$item->cost}}</span>
                                      @else 
                                          {{$item->cost}}<i class="lari lari-normal"></i>
                                      @endif
                                      
                                    </div>
                                  </div>
                                </div>
                              @endforeach
                        </div>
                        <hr class="divider divider-offset-lg divider-gray veil-sm reveal-md-block">
                    </div>
                    <div class="cell-sm-6 cell-md-12 offset-top-0 offset-sm-top-45 cell-md-push-7">
                        <div class="well text-center">
                            <h3>About <br class="veil reveal-lg-block"> Our Store</h3>
                            <hr class="divider divider-bold divider-base">
                            <h5 class="text-uppercase text-spacing-0">We are offering you the unique goods because our product is the real treasure.</h5>
                            <p class="offset-top-30">If you are a true fan, you'll love it. We have a tremendous variety in comparison to all of our competitors. Our collection is like a sea pearl among simple stones. Our devoted clients have noticed that our goods are the index of true, elegant taste. </p>
                        </div>
                    </div>
                    <div class="cell-sm-6 cell-md-12 cell-md-push-9 offset-top-45">
                        <div class="well text-center">
                            <h4>Follow us</h4>
                            <p class="offset-top-10">Read our latest news on any of these social networks!</p>
                            <ul class="offset-top-4 elements-group-18 reveal-inline-block text-middle">
                                <li><a href="javascript:void(0)" class="text-gray icon icon-xs fa-facebook"></a></li>
                                <li><a href="javascript:void(0)" class="text-gray icon icon-xs fa-twitter"></a></li>
                                <li><a href="javascript:void(0)" class="text-gray icon icon-xs fa-google-plus"></a></li>
                                <li><a href="javascript:void(0)" class="text-gray icon icon-xs fa-linkedin"></a></li>
                                <li><a href="javascript:void(0)" class="text-gray icon icon-xs fa-pinterest"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="rd-mailform-validate"></div>
    </div>
</main>


@endsection