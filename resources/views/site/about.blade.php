@extends('layouts.site_master')
@section('content')

<main class="page-content">
    <div class="shell">
        <div>
            <ol class="breadcrumb">
                <li><a href="{{ url('./') }}" class="icon icon-sm fa-home text-primary"></a></li>
                <li class="active">ჩვენს შესახებ</li>
            </ol>
        </div>
    </div>
    <div class="shell section-bottom-60">
        <div class="range">
           <div class="cell-md-8 text-xs-left">
                <h4>We want to be a diamond brand first of all</h4>
                <p class="text-bold">With the company's history dating 10 years back, we always felt desire to tell some kind of complete story with our jewelry. So the original style mixed with an androgynous feel of a modern day makes us what we actually are.
                </p>
                <img alt="" src="images/about-01.jpg" width="770" height="562" class="img-responsive offset-top-20">
                <p>We like the design part of the jewelry as much as you do. We are also sure that the visual appeal of any jewelry item can be easily integrated with the practicality and a balance can be achieved.
                </p>
                <p class="text-italic">Turquoise aims to bring the best in jewelry design to you, from all over the world across all the styles and cultures.
                </p>
                <img alt="" src="images/about-02.jpg" width="770" height="562" class="img-responsive offset-top-45">
                <h4 class="offset-top-20">Company's history</h4>
                <p>Turquoise is an online jewelry retailer with a unique approach and an undeniably stylish look. Born back in 2006, with founders team based in Boston, USA. Such a group of like-minded design professionals in one place is destined to make the result of such a work to be bright and exciting.
                </p>
                <img alt="" src="images/about-03.jpg" width="770" height="562" class="img-responsive offset-top-20">
                <h4 class="offset-top-20">Our Vision</h4>
                <p>It's really simple – we aim to make our products stylish, original and accessible – making materials quality and durability a vocal point. We produce everything we do with a grain of love into it – and our customers can always feel that in our end product.
                </p>
                <img alt="" src="images/about-04.jpg" width="770" height="562" class="img-responsive offset-top-20">
                <h4 class="offset-top-20">Our Team</h4>
                <p>The most innovative and talented designers in Boston are united under our banners – with each of their individual vision and creativity complimenting one another.
                </p>
            </div>

            <div class="cell-md-3 cell-md-preffix-1 sidebar sidebar-right text-md-left">
                <div class="range">
                    <div class="cell-md-12 cell-sm-6 offset-top-0 offset-sm-top-45 offset-md-top-0">
                    <h4>Categories</h4>
                        <ul class="offset-top-20 list-dividers">
                            <li><a href="{{ url('products/earring')}}">საყურეები</a></li>
                            <li><a href="{{ url('products/necklace')}}">ყელსაბამები</a></li>
                            <li><a href="{{ url('products/brooches')}}">გულსაკიდები</a></li>
                            <li><a href="{{ url('products/ring')}}">ბეჭდები</a></li>
                            <li><a href="{{ url('products/bracelet')}}">სამაჯური</a></li>
                            <li><a href="{{ url('products/exclusive')}}">ექსკლუზივი</a></li>                      
                        </ul>
                        <hr class="divider divider-offset-lg divider-gray veil-sm reveal-md-block">
                    </div>

                    <div class="cell-md-12 offset-top-0 offset-sm-top-45 text-left offset-md-top-0">
                        <h4 class="text-center text-md-left">Recent Reviews</h4>
                        <div class="range offset-top-20">

                            @foreach ($recent_show as $item)
                                <div class="offset-top-20 unit unit-horizontal unit-spacing-21">
                                  <div class="unit-left">
                                    <a href="{{ url('products/show/' . $item->id . '/item') }}"><img alt="" src="{{ url('item/img/' . $item->id . '/' . $item->photos->first()->fileName) }}" width="100" height="100"></a>
                                  </div>
                                  <div class="unit-body">
                                    <div class="p">
                                      <a href="{{url('products/' . $item->category)}}">{{$item->category}}</a>
                                    </div>
                                    <div class="big offset-top-4">
                                      <a href="{{url('products/show/' . $item->id . '/item')}}" class="text-base">{{$item->name}}</a></div>
                                    <div class="offset-top-4">

                                      @if ($item->sale)
                                          {{$item->sale}}<i class="lari lari-normal"></i>
                                          <span class="font-default text-light text-muted text-strike small">{{$item->cost}}</span>
                                      @else 
                                          {{$item->cost}}<i class="lari lari-normal"></i>
                                      @endif
                                      
                                    </div>
                                  </div>
                                </div>
                              @endforeach

                        </div>
                        <hr class="divider divider-offset-lg divider-gray veil-sm reveal-md-block">
                    </div>

                    <div class="cell-sm-6 cell-md-12 offset-top-0 offset-sm-top-45 cell-md-push-7">
                        <div class="well text-center">
                            <h3>About <br class="veil reveal-lg-block"> Our Store</h3>
                            <hr class="divider divider-bold divider-base">
                            <h5 class="text-uppercase text-spacing-0">We are offering you the unique goods because our product is the real treasure.</h5>
                            <p class="offset-top-30">If you are a true fan, you'll love it. We have a tremendous variety in comparison to all of our competitors. Our collection is like a sea pearl among simple stones. Our devoted clients have noticed that our goods are the index of true, elegant taste. </p>
                        </div>
                    </div>
                    <div class="cell-sm-6 cell-md-12 cell-md-push-9 offset-top-45">
                        <div class="well text-center">
                            <h4>Follow us</h4>
                            <p class="offset-top-10">Read our latest news on any of these social networks!</p>
                            <ul class="offset-top-4 elements-group-18 reveal-inline-block text-middle">
                                <li><a href="javascript:void(0)" class="text-gray icon icon-xs fa-facebook"></a></li>
                                <li><a href="javascript:void(0)" class="text-gray icon icon-xs fa-twitter"></a></li>
                                <li><a href="javascript:void(0)" class="text-gray icon icon-xs fa-google-plus"></a></li>
                                <li><a href="javascript:void(0)" class="text-gray icon icon-xs fa-linkedin"></a></li>
                                <li><a href="javascript:void(0)" class="text-gray icon icon-xs fa-pinterest"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="rd-mailform-validate"></div>
            </div>
        </div>
    </div>
</main>
@endsection