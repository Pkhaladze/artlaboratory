@extends('layouts.site_master')
@section('content')
    <main class="page-content">
        <section data-slide-effect="fade" data-min-height="430px" class="swiper-container swiper-slider swiper-container-horizontal swiper-container-fade">
            <div class="swiper-wrapper" style="transition-duration: 0ms;">
                <div data-slide-bg="images/index-03.jpg" class="bg-position-center-sm slide-mobile-overlay swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2" style="width: 638px; transform: translate3d(0px, 0px, 0px); transition-duration: 0ms; opacity: 1; background-image: url(&quot;images/index-03.jpg&quot;); background-size: cover;">
                    <div class="swiper-slide-caption text-sm-left">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-offset-6 col-sm-6 col-lg-offset-5 col-lg-8">
                                    <hr data-caption-animate="fadeInLeft" class="divider divider-base divider-sm-left divider-bold not-animated">
                                    <h1 data-caption-delay="100" data-caption-animate="fadeInUp" class="not-animated">Enamel Lifestory</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-slide-bg="images/index-01.jpg" class="slide-mobile-overlay swiper-slide" data-swiper-slide-index="0" style="width: 638px; transform: translate3d(-638px, 0px, 0px); transition-duration: 0ms; opacity: 1; background-image: url(&quot;images/index-01.jpg&quot;); background-size: cover;">
                    <div class="swiper-slide-caption">
                        <div class="container text-sm-left">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-6 col-md-5 col-md-offset-7">
                                    <hr data-caption-animate="fadeInLeft" class="divider divider-primary divider-sm-left divider-bold not-animated">
                                    <h1 data-caption-delay="100" data-caption-animate="fadeInUp" class="offset-top-30 not-animated">Enamel</h1>
                                    <h2 data-caption-delay="600" data-caption-animate="fadeInUp" class="not-animated">Lifestyle</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-slide-bg="images/index-02.jpg" class="bg-position-left bg-position-center slide-mobile-overlay swiper-slide" data-swiper-slide-index="1" style="width: 638px; transform: translate3d(-1276px, 0px, 0px); transition-duration: 0ms; opacity: 1; background-image: url(&quot;images/index-02.jpg&quot;); background-size: cover;">
                    <div class="swiper-slide-caption text-sm-left">
                        <div class="container">
                            <hr data-caption-animate="fadeInDown" class="divider-sm-left divider divider-base divider-bold not-animated">
                            <h1 data-caption-delay="300" data-caption-animate="fadeInUp" class="offset-top-30 not-animated">Retailer Enamel:</h1>
                            <h2 data-caption-delay="300" data-caption-animate="fadeInUp" class="not-animated">Brand Store</h2>
                        </div>
                    </div>
                </div>
                <div data-slide-bg="images/index-03.jpg" class="bg-position-center-sm slide-mobile-overlay swiper-slide swiper-slide-prev" data-swiper-slide-index="2" style="width: 638px; transform: translate3d(-1914px, 0px, 0px); transition-duration: 0ms; opacity: 1; background-image: url(&quot;images/index-03.jpg&quot;); background-size: cover;">
                    <div class="swiper-slide-caption text-sm-left">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-offset-6 col-sm-6 col-lg-offset-5 col-lg-8">
                                    <hr data-caption-animate="fadeInLeft" class="divider divider-base divider-sm-left divider-bold not-animated">
                                    <h1 data-caption-delay="100" data-caption-animate="fadeInUp" class="not-animated">Enamel Lifestory</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-slide-bg="images/index-01.jpg" class="slide-mobile-overlay swiper-slide swiper-slide-duplicate swiper-slide-active" data-swiper-slide-index="0" style="width: 638px; transform: translate3d(-2552px, 0px, 0px); transition-duration: 0ms; opacity: 1; background-image: url(&quot;images/index-01.jpg&quot;); background-size: cover;">
                    <div class="swiper-slide-caption">
                        <div class="container text-sm-left">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-6 col-md-5 col-md-offset-7">
                                    <hr data-caption-animate="fadeInLeft" class="divider divider-primary divider-sm-left divider-bold fadeInLeft animated">
                                    <h1 data-caption-delay="100" data-caption-animate="fadeInUp" class="offset-top-30 fadeInUp animated">Enamel</h1>
                                    <h2 data-caption-delay="600" data-caption-animate="fadeInUp" class="fadeInUp animated">Lifestyle</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <!-- Swiper Navigation-->
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
            <!-- Swiper Pagination-->
            <div class="swiper-pagination swiper-pagination-clickable">
                <span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span>
                <span class="swiper-pagination-bullet"></span>
                <span class="swiper-pagination-bullet"></span>
            </div>
        </section>
        <section class="section-top-60">
            <div class="shell">
                <h3>Browse Our Categories</h3>
                <hr class="divider divider-base divider-bold">
                <div class="range offset-top-30">
                    <div class="cell-md-3 cell-xs-6">
                        <a href="{{ url('products/earring')}}" class="thumbnail-variant-1">
                            <img alt="" src="images/index-08.jpg" width="270" height="363" class="img-responsive">
                            <div class="caption">
                                <h5 class="caption-title">საყურეები</h5>
                                <p class="caption-descr">                      
                                    {{$items->where('category', 'earring')->count()}} პროდუქტი
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="offset-top-30 offset-xs-top-0 cell-md-3 cell-xs-6">
                        <a href="{{ url('products/necklace')}}" class="thumbnail-variant-1">
                            <img alt="" src="images/index-09.jpg" width="270" height="363" class="img-responsive">
                            <div class="caption">
                                <h5 class="caption-title">ყელსაბამები</h5>
                                <p class="caption-descr">                      
                                    {{$items->where('category', 'necklace')->count()}} პროდუქტი
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="offset-top-30 offset-md-top-0 cell-md-3 cell-xs-6">
                        <a href="{{ url('products/brooches')}}" class="thumbnail-variant-1">
                            <img alt="" src="images/index-10.jpg" width="270" height="363" class="img-responsive">
                            <div class="caption">
                                <h5 class="caption-title">გულსაკიდები</h5>
                                <p class="caption-descr">
                                    {{$items->where('category', 'brooches')->count()}} პროდუქტი
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="offset-top-30 offset-md-top-0 cell-md-3 cell-xs-6">
                        <a href="{{ url('products/ring') }}" class="thumbnail-variant-1">
                            <img alt="" src="images/index-11.jpg" width="270" height="363" class="img-responsive">
                            <div class="caption">
                                <h5 class="caption-title">ბეჭდები</h5>
                                <p class="caption-descr">
                                    {{$items->where('category', 'ring')->count()}} პროდუქტი
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-60">
            <div class="shell text-left">
                <div class="range">

                    @include('includes.newest_item')

                    @include('includes.sale_item')

                    @include('includes.top_rate')

                </div>
            </div>
        </section>
        <section class="offset-top-45">
            <div style="background-image: url(images/index-16.jpg); background-repeat: no-repeat; background-size: cover;" class="shell well-variant-1">
                <h3>ჩვენი მაღაზიის შესახებ</h3>
                <hr class="divider divider-base divider-bold">
                <p class="text-regular text-uppercase">
                    We are offering you the unique goods because our product is the real treasure.
                </p>
                <p class="offset-top-20">If you are a true fan, you’ll love it. We have a tremendous variety in comparison to all of our competitors. Our collection is like a sea pearl among simple stones. Our devoted clients have noticed that our goods are the index of true, elegant taste.</p>
                <p></p>
            </div>
        </section>
    </main>
@endsection