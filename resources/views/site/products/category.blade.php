@extends('layouts.site_master')
@section('content')

<main class="page-content">
  <div class="shell">
    <div>
      <ol class="breadcrumb">
       	<li><a href="{{ url('./') }}" class="icon icon-sm fa-home text-primary"></a></li>
       	<li class="active">პროდუქტები</li>
      </ol>
    </div>
  </div>

  @include('includes.item_category')

</main>
@endsection