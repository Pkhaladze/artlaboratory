@extends('layouts.site_master')

@section('content')
<main class="page-content"></main>
<div class="shell section-60">
    <div class="range product-details">
        <div class="cell-md-6">
            <div class="range">
              <div class="cell-lg-2 cell-sm-2 cell-md-3">
                <div class="slider-nav">
                @for ($i = 0; $i < 5; $i++)
                    <div>
                      <img alt="" src="{{url('item/img/' . $item->id . '/' . $item->photos->get($i)->fileName)}}" width="88" height="88">
                  </div>
                @endfor
                </div>
              </div>
              <div class="cell-lg-10 cell-md-9 cell-sm-8 offset-top-20 offset-sm-top-0">
                <div data-lightbox="gallery" class="slider-for">
                  @for ($i = 0; $i < 5; $i++)
                  <div>
                    <div class="thumbnail-variant-2">
                      <img src="{{url('item/img/' . $item->id . '/' . $item->photos->get($i)->fileName)}}" width="470" height="632" alt="" class="img-responsive">
                      <div class="caption">
                        <a href="category.html" class="label label-primary">Sale</a></div>
                      <div class="caption-variant-1">
                        <a data-lightbox="image" href="{{url('item/img/' . $item->id . '/' . $item->photos->get($i)->fileName)}}" class="icon icon-base icon-circle fl-line-icon-set-magnification3"></a></div>
                    </div>
                  </div>
                  @endfor
                </div>
              </div>
            </div>
          </div>
        <div class="cell-md-6 text-sm-left">
            <h3 class="product-details-heading text-thin">{{$item->name}}</h3>
            <div class="offset-top-4 reveal-sm-flex range-sm-justify">
              <div class="product-details-price">
              @if ($item->sale)                
				        <span class="product-details-price-big">{{$item->sale}}
                  <i class="lari lari-normal"></i>
                </span>
              	<span class="product-details-price-small text-strike text-muted">{{$item->cost}}
                  <i class="lari lari-normal"></i>
                </span>
              @else
                <span class="product-details-price-big">{{$item->cost}}
                  <i class="lari lari-normal"></i>
                </span>
              @endif
              </div>
            </div>
            <hr class="divider divider-iron divider-dotted divider-offset-20">
            <p>
              <span class="text-italic">კატეგორიები:</span>
              <a href="{{ url('products/earring')}}">საყურეები,</a>
              <a href="{{ url('products/necklace')}}">ყელსაბამები,</a>
              <a href="{{ url('products/brooches')}}">გულსაკიდები,</a>
              <a href="{{ url('products/ring')}}">ბეჭდები,</a>
              <a href="{{ url('products/bracelet')}}">სამაჯური,</a>
              <a href="{{ url('products/exclusive')}}">ექსკლუზივი</a>
            <p>
              <span class="text-italic">Tag:</span>
              <a href="{{ url('products/' . $item->category)}}">{{$item->category }}</a>
            </p>
            <hr class="offset-top-20 divider divider-iron divider-dotted">
            <ul class="offset-top-10 elements-group-18 reveal-inline-block text-middle">
              <li><a href="#" class="text-gray icon icon-xs fa-facebook"></a></li>
              <li><a href="#" class="text-gray icon icon-xs fa-twitter"></a></li>
              <li><a href="#" class="text-gray icon icon-xs fa-google-plus"></a></li>
              <li><a href="#" class="text-gray icon icon-xs fa-linkedin"></a></li>
              <li><a href="#" class="text-gray icon icon-xs fa-pinterest"></a></li>
            </ul>
          </div>
        </div>
        <!-- Responsive-tabs-->
        <div class="responsive-tabs offset-top-30" style="display: block; width: 100%;">
          <ul class="resp-tabs-list">
            <li class="resp-tab-item resp-tab-active" aria-controls="tab_item-0" role="tab">Description</li>
          </ul>
          <div class="resp-tabs-container text-left">
            <div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
              
              <p class="offset-top-20">{{$item->description}}</p>
            </div>
          </div>
      </div>

@endsection