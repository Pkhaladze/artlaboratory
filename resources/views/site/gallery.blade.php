@extends('layouts.site_master')
@php
    use App\Models\Item;
    $rate_items = Item::where('status', 'active')->orderBy('rate', 'desc')->take(3)->get();
    $newest_items = Item::where('status', 'active')->orderBy('id', 'desc')->take(3)->get();
@endphp
@section('content')
<main class="page-content">
        <div class="shell">
          <div>
            <ol class="breadcrumb">
              <li><a href="{{ url('./')}}" class="icon icon-sm fa-home text-primary"></a></li>
                <li class="active">გალერეა</li>
            </ol>
          </div>
        </div>
        <div class="shell section-bottom-60">
          <div class="range">
            <div class="cell-md-9">
                <div class="range">
                  <div class="cell-md-4">
                    <div class="form-group select-filter">
                      <select class="select2 ">
                        <option>Default sorting</option>
                        <option>Sort by popularity</option>
                        <option>Sort by average rating</option>
                        <option>Sort by newness</option>
                        <option>Sort by price: low to high</option>
                        <option>Sort by price: high to low</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-md-4 cell-middle text-md-left">
                    <h6>Showing all <span class="text-primary">{{ $items->count() }} results</span></h6>
                  </div>
                </div>
              <div class="range offset-top-30">
                @foreach ($items as $item)
                <div class="cell-md-4 cell-sm-6">
                  <div class="product reveal-inline-block">
                    <div class="product-media">
                      <a href="{{ url('products/show/' . $item->id . '/item') }}">
                        <img alt="" src="{{ url('item/img/' . $item->id . '/' . $item->photos->first()->fileName) }}" width="290" height="389" class="img-responsive">
                      </a>                      
                    </div>
                    <div class="offset-top-10">
                      <p class="big">
                        <a href="{{ url('products/show/' . $item->id . '/item') }}" class="text-base">{{$item->name}}</a>
                      </p>
                    </div>
                    <div class="product-price text-bold">
                      @if ($item->sale)
                          {{$item->sale}}<i class="lari lari-normal"></i>
                          <span class="font-default text-light text-muted text-strike small">{{$item->cost}}</span>
                      @else 
                          {{$item->cost}}<i class="lari lari-normal"></i>
                      @endif
                    </div>
                  </div>
                </div>
                @endforeach
                <!-- -->
              </div>
              <div class="text-md-left offset-top-45">
              {{-- {{ $items->links() }} --}}
<!--                 <ul class="pagination">
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li class="next"><a href="#" class="mdi mdi-chevron-right"></a></li>
                </ul> -->
              </div>
            </div>
            <div class="offset-top-60 offset-md-top-0 cell-md-3 text-left">
              <div class="range">
                <div class="cell-md-12 cell-sm-6">
                  <h4>Browse</h4>
                  <ul class="offset-top-20 list-dividers">
                    <li><a href="{{ url('products/earring')}}">საყურეები</a></li>
                    <li><a href="{{ url('products/necklace')}}">ყელსაბამები</a></li>
                    <li><a href="{{ url('products/brooches')}}">გულსაკიდები</a></li>
                    <li><a href="{{ url('products/ring')}}">ბეჭდები</a></li>
                    <li><a href="{{ url('products/bracelet')}}">სამაჯური</a></li>
                    <li><a href="{{ url('products/exclusive')}}">ექსკლუზივი</a></li>
                  </ul>
                  <hr class="divider divider-offset-lg divider-gray veil reveal-md-block">
                </div>
                <div class="cell-md-12 cell-sm-6 offset-top-45 offset-md-top-0 text-left">
                  <h4>Top Rated Products</h4>
                  @foreach ($rate_items as $item)
                  <div class="offset-top-20 unit unit-horizontal unit-spacing-21">
                    <div class="unit-left">
                      <a href="{{ url('products/show/' . $item->id . '/item') }}"><img alt="" src="{{ url('item/img/' . $item->id . '/' . $item->photos->first()->fileName) }}" width="100" height="100"></a>
                    </div>
                    <div class="unit-body">
                      <div class="p">
                        <a href="{{url('products/' . $item->category)}}">{{$item->category}}</a>
                      </div>
                      <div class="big offset-top-4">
                        <a href="{{url('products/show/' . $item->id . '/item')}}" class="text-base">{{$item->name}}</a></div>
                      <div class="offset-top-4">

                        @if ($item->sale)
                            {{$item->sale}}<i class="lari lari-normal"></i>
                            <span class="font-default text-light text-muted text-strike small">{{$item->cost}}</span>
                        @else 
                            {{$item->cost}}<i class="lari lari-normal"></i>
                        @endif
                        
                      </div>
                    </div>
                  </div>
                  @endforeach
                  <hr class="divider divider-offset-lg divider-gray veil reveal-md-block">
                </div>

                <div class="cell-md-12 cell-sm-6 offset-top-45 offset-md-top-0 text-left">
                  <h4>ახალი პროდუქტი</h4>
                  @foreach ($newest_items as $item)
                    <div class="offset-top-20 unit unit-horizontal unit-spacing-21">
                      <div class="unit-left">
                        <a href="{{ url('products/show/' . $item->id . '/item') }}"><img alt="" src="{{ url('item/img/' . $item->id . '/' . $item->photos->first()->fileName) }}" width="100" height="100"></a>
                      </div>
                      <div class="unit-body">
                        <div class="p">
                          <a href="{{url('products/' . $item->category)}}">{{$item->category}}</a>
                        </div>
                        <div class="big offset-top-4">
                          <a href="{{url('products/show/' . $item->id . '/item')}}" class="text-base">{{$item->name}}</a></div>
                        <div class="offset-top-4">

                          @if ($item->sale)
                              {{$item->sale}}<i class="lari lari-normal"></i>
                              <span class="font-default text-light text-muted text-strike small">{{$item->cost}}</span>
                          @else 
                              {{$item->cost}}<i class="lari lari-normal"></i>
                          @endif
                          
                        </div>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>

@endsection