<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Store;
use App\Models\Photo;
use Validator;
use Storage;
use DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        return view('admin.index', compact('items'));
    }

    /**s
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::all();
        $stores = Store::all();
        return view('admin.edit_create', compact('items', 'stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:50',
            'weight' => 'required',
            'width' => 'required',
            'length' => 'required',
            'cost' => 'required',
            'material' => 'required',
            'author' => 'required',
            'store_id' => 'required',
            'category' => 'required',
            'status' => 'required',
            'description' => 'required',
            'photo-1' => 'required',
            'photo-2' => 'required',
            'photo-3' => 'required',
            'photo-4' => 'required',
            'photo-5' => 'required',
        ];

        try {
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput($request->all())
                    ->withErrors($validator);
            }
            DB::beginTransaction();
            $item = new Item;
            $item->name = $request->name;
            $item->weight = $request->weight;
            $item->width = $request->width;
            $item->length = $request->length;
            $item->cost = $request->cost;
            $item->sale = $request->sale == '' ? null : $request->sale;
            $item->material = $request->material;
            $item->author = $request->author;
            $item->store_id = $request->store_id;
            $item->category = $request->category;
            $item->status = $request->status;
            $item->description = $request->description;
            $item->save();
            for ($i = 1; $i <= 5; $i++) {
                $file = $request->file('photo-' . $i);
                $ext = $file->guessClientExtension();
                $imageName = time() . str_random(3) . '.' . $ext;

                $photo = new Photo;
                $photo->fileName = $imageName;
                $photo->item_id = $item->id;
                $photo->save();

                $file->storeAs('item/img/' . $item->id . '/', $photo->fileName);
                $filePath = 'item/img/' . $item->id . '/' . $photo->fileName;
                Storage::put($filePath, file_get_contents($file), 'public');
            }
            DB::commit();
        } catch (Exception $e) {
                DB::rollBack();
            }

        return redirect()->back()->with('success', "წარმატებით დაემატა ბაზაში!");
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $stores = Store::all();
        //dd($item->description);
        return view('admin.edit_create', compact('item', 'stores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:50',
            'weight' => 'required',
            'width' => 'required',
            'length' => 'required',
            'cost' => 'required',
            'material' => 'required',
            'author' => 'required',
            'store_id' => 'required',
            'category' => 'required',
            'status' => 'required',
            'description' => 'required',
        ];

        try {
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput($request->all())
                    ->withErrors($validator);
            }
            $item = Item::find($id);
            $item->name = $request->name;
            $item->weight = $request->weight;
            $item->width = $request->width;
            $item->length = $request->length;
            $item->cost = $request->cost;
            $item->sale = $request->sale == '' ? null : $request->sale;
            $item->material = $request->material;
            $item->author = $request->author;
            $item->store_id = $request->store_id;
            $item->category = $request->category;
            $item->status = $request->status;
            $item->description = $request->description;
            $item->save();
            DB::commit();
        } catch (Exception $e) {
                DB::rollBack();
            }

        return redirect()->back()->with('success', "წარმატებით შესწორდა ბაზაში!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Item::where('id', $id)->delete();
        return redirect()->back();  
    }
}
