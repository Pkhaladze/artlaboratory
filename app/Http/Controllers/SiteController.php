<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use Validator;
use Illuminate\Validation\Rule;
use App\Models\Message;

class SiteController extends Controller
{
    public function index()
    {
        $items = Item::all();
        $active_link = 'home';
        return view('site.index', compact('items' , 'active_link'));
    }

    public function products()
    {
        $items = Item::where('status', 'active')->get();
        $active_link = 'products';
        return view('site.products.category', compact('items' , 'active_link'));
    }  

    public function material($material)
    {
        $active_link = 'products';
        $category = $material;
        $items = Item::where('status', 'active')->where('material', $material)->orderBy('id', 'desc')->get();
        $rate_items = Item::where('status', 'active')->orderBy('rate', 'desc')->take(3)->get();
        $newest_items = Item::where('status', 'active')->orderBy('id', 'desc')->take(3)->get();
        return view('site.products.item_catalog', 
            compact('items', 'active_link', 'category', 'rate_items', 'newest_items')
            );
    }   

        public function material_category($material, $category)
    {
        $active_link = 'products';
        $items = Item::where('status', 'active')->where('material', $material)->where('category', $category)->orderBy('id', 'desc')->get();
        $rate_items = Item::where('status', 'active')->orderBy('rate', 'desc')->take(3)->get();
        $newest_items = Item::where('status', 'active')->orderBy('id', 'desc')->take(3)->get();
        return view('site.products.item_catalog', 
            compact('items', 'active_link', 'category', 'rate_items', 'newest_items', 'material')
            );
    }   

    public function show($id)
    {
        $item = Item::find($id);
        $item->rate = $item->rate + 1;
        $item->save();
        $active_link = 'products';
        return view('site.products.show', compact('item' , 'active_link'));
    }  

    public function catalog($category)
    {
        $active_link = 'products';
        
        $categories [] = $category;
        $Validation = Validator::make($categories, [
            [
                'required',
                Rule::in(['earring', 'ring', 'necklace', 'brooches', 'bracelet', 'exclusive']),
            ],
        ]);   
        if ($Validation->fails()) {
                return redirect("./");
            }

        $items = Item::where('status', 'active')->where('category' , $category)->orderBy('id', 'desc')->get();
        $rate_items = Item::where('status', 'active')->orderBy('rate', 'desc')->take(3)->get();
        $newest_items = Item::where('status', 'active')->orderBy('id', 'desc')->take(3)->get();
        return view('site.products.item_catalog', 
            compact('items', 'active_link', 'category', 'rate_items', 'newest_items')
            );
    }

        public function gallery()
    {
        $items = Item::all();
        $active_link = 'gallery';
        return view('site.gallery', compact('items' , 'active_link'));
    }


        public function about()
    {
        $active_link = 'about';
        $recent_show =Item::orderBy('updated_at', 'desc')->take(3)->get();
        return view('site.about', compact('active_link', 'recent_show'));
    }

        public function contacts()
    {
        $active_link = 'contact';
        $recent_show =Item::orderBy('updated_at', 'desc')->take(3)->get();
        return view('site.contacts', compact('active_link', 'recent_show'));
    } 
        public function message(Request $request)
    {
        $rules = [
            'name' => 'required|max:30',
            'email' => 'required|max:30',
            'question' => 'required',
        ];

        try {
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput($request->all())
                    ->withErrors($validator);
            }
            $message = new Message;
            $message->name = $request->name;
            $message->email = $request->email;
            $message->question = $request->question;
            $message->save(); 
        } catch (Exception $e)   {

        }         
        return redirect()->back()->with('success', "მესიჯი წარმატებით გაიგზავნა");
    } 
}
