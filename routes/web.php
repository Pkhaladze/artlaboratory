<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'SiteController@index');
Route::get('/products', 'SiteController@products');
Route::get('/products/{catalog}', 'SiteController@catalog');
Route::get('/products/show/{id}/item', 'SiteController@show');
Route::get('/products/catalog/{material}', 'SiteController@material');
Route::get('/products/{material}/{category}', 'SiteController@material_category');
Route::get('/gallery', 'SiteController@gallery');
Route::get('/about', 'SiteController@about');
Route::get('/contacts', 'SiteController@contacts');
Route::post('/message', 'SiteController@message');

Auth::routes();
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index');
    Route::post('/items/create/upload', 'FileUploadController@upload');
    Route::resource('/items', 'AdminController');

});

/*image read*/
Route::get('item/img/{id}/{filename}', function ($id, $filename)
{
    $path = storage_path() . '/app/item/img/'. $id . '/' . $filename;


    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
