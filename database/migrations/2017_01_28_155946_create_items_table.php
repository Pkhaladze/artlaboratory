<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->unique();
            $table->float('weight')->unsigned();
            $table->float('length')->unsigned();
            $table->float('width')->unsigned();
            $table->float('cost')->unsigned();
            $table->float('sale')->nullable();
            $table->integer('rate')->nullable();
            $table->enum('status',['active', 'passive'])->default('active');
            $table->enum('material',['gold', 'silver', 'enamel'])->default('enamel');
            $table->enum('author',['Tamuna Tsnoreli', 'Natia Nebieridze'])->default('Tamuna Tsnoreli');
            $table->integer('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
            $table->enum('category',['earring', 'necklace', 'brooches', 'ring', 'bracelet', 'exclusive']);
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
