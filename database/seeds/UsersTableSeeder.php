<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            [
            'name' => 'Merab Pkhaladze',
            'email' => 'merabpkhaladze@gmail.com',
            'password' => bcrypt('AS#$tyui'),
            ],
            [
            'name' => 'TamunaTsnoreli',
            'email' => 'ttsnoreli@gmail.com',
            'password' => bcrypt('Ttsnoreli^&'),
            ],
        ]
        );
    }
}
